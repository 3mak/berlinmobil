<?php
/**
 * Plugin Name: Berlinmobil
 * Plugin URI:  http://berlinmobil.de
 * Description: Berlinmobil Plugins
 * Version:     0.0.2
 * Author:      Erstellbar
 * Author URI:  https://erstellbar.de
 * Donate link: http://berlinmobil.de
 * License:     GPLv2
 * Text Domain: berlinmobil
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2016 Erstellbar (email : sven@erstellbar.de)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Built using generator-plugin-wp
 */


// User composer autoload.
require __DIR__ . '/vendor/autoload.php';

define( 'BERLINMOBIL_MODULES_DIR', plugin_dir_path( __FILE__ ) . 'includes/beaver-modules/' );
define( 'BERLINMOBIL_MODULES_URL', plugins_url( 'includes/beaver-modules/', __FILE__ ) );


/**
 * Main initiation class
 *
 * @since  NEXT
 * @var  string $version  Plugin version
 * @var  string $basename Plugin basename
 * @var  string $url      Plugin URL
 * @var  string $path     Plugin Path
 */
class Berlinmobil {

	/**
	 * Current version
	 *
	 * @var  string
	 * @since  NEXT
	 */
	const VERSION = '0.0.2';

	/**
	 * URL of plugin directory
	 *
	 * @var string
	 * @since  NEXT
	 */
	protected $url = '';

	/**
	 * Path of plugin directory
	 *
	 * @var string
	 * @since  NEXT
	 */
	protected $path = '';

	/**
	 * Plugin basename
	 *
	 * @var string
	 * @since  NEXT
	 */
	protected $basename = '';

	/**
	 * Singleton instance of plugin
	 *
	 * @var Berlinmobil
	 * @since  NEXT
	 */
	protected static $single_instance = null;

	/**
	 * Instance of B_Vehicle
	 *
	 * @since NEXT
	 * @var B_Vehicle
	 */
	protected $vehicle;


	/**
	 * Instance of B_Theme
	 *
	 * @since NEXT
	 * @var B_Theme
	 */
	protected $theme;

	/**
	 * Instance of B_Fleet
	 *
	 * @since NEXT
	 * @var B_Fleet
	 */
	protected $fleet;

	/**
	 * Instance of B_Fleet_Page
	 *
	 * @since NEXT
	 * @var B_Fleet_Page
	 */
	protected $fleet_page;

	/**
	 * Instance of B_Shortcodes
	 *
	 * @since NEXT
	 * @var B_Shortcodes
	 */
	protected $shortcodes;

	/**
	 * @var B_Author_Module
	 */
	protected $team_module;

	/**
	 * @var B_Fadebox_Module
	 */
	protected $fade_box;

	/**
	 * @var B_Trailer
	 */
	protected $trailer;

	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  NEXT
	 * @return Berlinmobil A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$single_instance ) {
			self::$single_instance = new self();
		}

		return self::$single_instance;
	}

	/**
	 * Sets up our plugin
	 *
	 * @since  NEXT
	 */
	protected function __construct() {
		$this->basename = plugin_basename( __FILE__ );
		$this->url      = plugin_dir_url( __FILE__ );
		$this->path     = plugin_dir_path( __FILE__ );

		$this->plugin_classes();
	}

	/**
	 * Attach other plugin classes to the base plugin class.
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function plugin_classes() {
		include_once dirname(__FILE__) . "/vendor/cmb2/init.php";
		include_once dirname(__FILE__) . "/vendor/cmb2-taxonomy/init.php";
		
		// Attach other plugin classes to the base plugin class.
		$this->vehicle = new B_Vehicle( $this );
		$this->fleet = new B_Fleet( $this );
		$this->fleet_page = new B_Fleet_Page( $this );
		$this->shortcodes = new B_Shortcodes( $this );
		$this->trailer = new B_Trailer($this);
	} // END OF PLUGIN CLASSES FUNCTION

	/**
	 * Add hooks and filters
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
		add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * Activate the plugin
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function _activate() {
		// Make sure any rewrite functionality has been loaded.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate the plugin
	 * Uninstall routines should be in uninstall.php
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function _deactivate() {}

	/**
	 * Init hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function init() {
		if ( $this->check_requirements() ) {
			load_plugin_textdomain( 'berlinmobil', false, dirname( $this->basename ) . '/languages/' );

			$this->team_module = new B_Author_Module();
			$this->fade_box = new B_Fadebox_Module();
		}
	}

	/**
	 * Check if the plugin meets requirements and
	 * disable it if they are not present.
	 *
	 * @since  NEXT
	 * @return boolean result of meets_requirements
	 */
	public function check_requirements() {
		if ( ! $this->meets_requirements() ) {

			// Add a dashboard notice.
			add_action( 'all_admin_notices', array( $this, 'requirements_not_met_notice' ) );

			// Deactivate our plugin.
			add_action( 'admin_init', array( $this, 'deactivate_me' ) );

			return false;
		}

		return true;
	}

	/**
	 * Deactivates this plugin, hook this function on admin_init.
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function deactivate_me() {
		deactivate_plugins( $this->basename );
	}

	/**
	 * Check that all plugin requirements are met
	 *
	 * @since  NEXT
	 * @return boolean True if requirements are met.
	 */
	public static function meets_requirements() {
		// Do checks for required classes / functions
		// function_exists('') & class_exists('').
		// We have met all requirements.
		return true;
	}

	/**
	 * Adds a notice to the dashboard if the plugin requirements are not met
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function requirements_not_met_notice() {
		// Output our error.
		echo '<div id="message" class="error">';
		echo '<p>' . sprintf( __( 'Berlinmobil is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available.', 'berlinmobil' ), admin_url( 'plugins.php' ) ) . '</p>';
		echo '</div>';
	}

	/**
	 * Magic getter for our object.
	 *
	 * @since  NEXT
	 * @param string $field Field to get.
	 * @throws Exception Throws an exception if the field is invalid.
	 * @return mixed
	 */
	public function __get( $field ) {
		switch ( $field ) {
			case 'version':
				return self::VERSION;
			case 'basename':
			case 'url':
			case 'path':
			case 'vehicle':
			default:
				throw new Exception( 'Invalid '. __CLASS__ .' property: ' . $field );
		}
	}
}

/**
 * Grab the Berlinmobil object and return it.
 * Wrapper for Berlinmobil::get_instance()
 *
 * @since  NEXT
 * @return Berlinmobil  Singleton instance of plugin class.
 */
function berlinmobil() {
	return Berlinmobil::get_instance();
}

// Kick it off.
add_action( 'plugins_loaded', array( berlinmobil(), 'hooks' ) );

register_activation_hook( __FILE__, array( berlinmobil(), '_activate' ) );
register_deactivation_hook( __FILE__, array( berlinmobil(), '_deactivate' ) );
