# Berlinmobil #
**Contributors:**      Erstellbar
**Donate link:**       http://berlinmobil.de
**Tags:**
**Requires at least:** 4.4
**Tested up to:**      4.4
**Stable tag:**        0.0.1
**License:**           GPLv2
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html

## Description ##

Berlinmobil Plugins

## Installation ##

### Manual Installation ###

1. Upload the entire `/berlinmobil` directory to the `/wp-content/plugins/` directory.
2. Activate Berlinmobil through the 'Plugins' menu in WordPress.

## Frequently Asked Questions ##


## Screenshots ##


## Changelog ##

### 0.0.1 ###
* First release

## Upgrade Notice ##

### 0.0.1 ###
First Release
