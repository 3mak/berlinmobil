��    /      �  C                *   %  
   P     [     b     i     |     �     �     �  	   �     �     �     �     �     �     �     �  
   �     �                         (     ?     L  	   ]  	   g     q  
   y     �     �     �     �     �     �     �     �     �     �                 
        %  �  *     �  0   �     �                    0     C  	   O     Y     g     s     �     �     �     �  	   �     �     �     �  
   �     �     �     �     	     	     1	     D	     M	     \	     h	     u	     �	     �	     �	  	   �	     �	     �	  	   �	     �	     �	  	   
     
     
  	   
     &
     ,            
                                   (   &                  )   $   !          /                                 .              "             -                       '          #             +   	   *           %             %1$s saved. %1$s updated. <a href="%2$s">View %1$s</a> Add New %s All %s Author Available Trailers Berlinmobil Modules Climatic Drinks Edit %s Equipment Fadebox File: Fleet Fleet Options Fleets General Infotainment Live Photo Music Navi New %s No %s No %s found in Trash Request Form Shortcode Save contact Scan QR<br> Code Search %s Seat view Seating Show Fleet Show selected fleet Suitability Team Member Toilett Trailer Trailer Number Trailer Options Trailers Vehicle Vehicle Properties Vehicles Video View %s Wheelchair Wifi Project-Id-Version: Berlinmobil 0.0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/berlinmobil
POT-Creation-Date: 2016-06-09 15:22+0200
PO-Revision-Date: 2016-06-09 15:23+0200
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n != 1);
 %1$s gespeichert. %1$s aktualisiert. <a href="%2$s">Zeige %1$s</a> %s erstellen Alle %s Autor Verfügbare Anhänger Berlinmobil Module Klimaanlage Getränke %s bearbeiten Ausstattung Box mit Verlauf Datei: Flotte Flottenoptionen Flotten Allgemein Infotainment Echtes Foto Musik Navigation Neue %s Keine %s Keine %s im Papierkorb Anfrageformular Shortcode Kontakt Speichern QR Code<br>scannen Suche %s Sitzübersicht Sitzplätze Zeige Flotte Zeige ausgewählte Flotte Eignung Teammitglied Toilette Anhänger Anhänger Nummer Anhängeroptionen Anhänger Fahrzeug Fahrzeugeigenschaften Fahrzeuge Video Zeige %s Rollstuhl WLAN 