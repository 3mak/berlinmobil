<?php

class B_Parallax_Beaver_Module_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists( 'B_Parallax_Beaver_Module') );
	}

	function test_class_access() {
		$this->assertTrue( berlinmobil()->parallax-beaver-module instanceof B_Parallax_Beaver_Module );
	}
}
