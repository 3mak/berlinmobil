<?php

class B_Fleet_Page_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists( 'B_Fleet_Page') );
	}

	function test_class_access() {
		$this->assertTrue( berlinmobil()->fleet-page instanceof B_Fleet_Page );
	}
}
