<?php

class B_Vehicle_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists( 'B_Vehicle') );
	}

	function test_class_access() {
		$this->assertTrue( berlinmobil()->vehicle instanceof B_Vehicle );
	}

  function test_cpt_exists() {
    $this->assertTrue( post_type_exists( 'b-vehicle' ) );
  }
}
