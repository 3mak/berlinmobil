<?php

class B_Shortcodes_Test extends WP_UnitTestCase {

	function test_sample() {
		// replace this with some actual testing code
		$this->assertTrue( true );
	}

	function test_class_exists() {
		$this->assertTrue( class_exists( 'B_Shortcodes') );
	}

	function test_class_access() {
		$this->assertTrue( berlinmobil()->shortcodes instanceof B_Shortcodes );
	}
}
