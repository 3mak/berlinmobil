<?php
/**
 * Berlinmobil Vehicle
 *
 * @version 0.0.1
 * @package Berlinmobil
 */

class B_Vehicle extends CPT_Core {
	/**
	 * Parent plugin class
	 *
	 * @var class
	 * @since  NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name.
		parent::__construct(
			array( __( 'Vehicle', 'berlinmobil' ), __( 'Vehicles', 'berlinmobil' ), 'b-vehicle' ),
			array( 'supports' => array( 'title', 'thumbnail' ) )
		);
	}

	public static function getEquipments() {
		return [
			'toilett' => __('Toilett', 'berlinmobil'),
			'wifi' => __('Wifi', 'berlinmobil'),
			'video' => __('Video', 'berlinmobil'),
			'navi'=> __('Navi', 'berlinmobil'),
			'drinks'=> __('Drinks', 'berlinmobil'),
			'music'=> __('Music', 'berlinmobil'),
			'climatic'=> __('Climatic', 'berlinmobil'),
			'wheelchair' => __('Wheelchair', 'berlinmobil'),
			'infotainment' => __('Infotainment', 'berlinmobil'),
		];
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
		add_action( 'cmb2_init', array( $this, 'fields' ) );
	}

	/**
	 * Add custom fields to the CPT
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function fields() {
		$prefix = 'b_vehicle_';

		$cmb = new_cmb2_box( array(
			'id'            => $prefix . 'outside',
			'title'         => __( 'Vehicle Properties', 'berlinmobil' ),
			'object_types'  => array( 'b-vehicle' ),
		) );

		$cmb->add_field(array(
			'id' => $prefix . 'trailer_ids',
			'name' => __('Available Trailers', 'berlinmobil'),
			'type'             => 'multicheck_inline',
			'select_all_button' => false,
			'options_cb' => array($this, 'show_trailer'),
		));

        $cmb->add_field( array(
            'id' => $prefix . 'live_photo',
            'name' => __('Live Photo', 'berlinmobil'),
            'type'  => 'file'
        ));

		$group_field_id = $cmb->add_field( array(
			'id'          => 'live_photo_slider',
			'type'        => 'group',
			'description' => __( 'Live Photo Slider (Overrides Live Photo!)', 'berlinmobil' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Photo {#}', 'berlinmobil' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add Another Photo', 'berlinmobil' ),
				'remove_button' => __( 'Remove Photo', 'berlinmobil' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		$cmb->add_group_field( $group_field_id, array(
			'id' => $prefix . 'live_photo_slide',
			'name' => __('Live Photo Slide', 'berlinmobil'),
			'type'  => 'file'
		) );

        $cmb->add_field( array(
            'id' => $prefix . 'seat_view',
            'name' => __('Seat view', 'berlinmobil'),
            'type'  => 'file'
        ));

		$cmb->add_field( array(
			'id' => $prefix . 'seating',
			'name' => __('Seating', 'berlinmobil'),
			'type'  => 'text'
		));


		$cmb->add_field( array(
			'id' => $prefix . 'suitability',
			'name' => __('Suitability', 'berlinmobil'),
			'type' => 'text',
		));

		$cmb->add_field( array(
			'id' => $prefix . 'panolink',
			'name' => __('Panorama Link', 'berlinmobil'),
			'type' => 'text',
		));

		$cmb->add_field( array(
			'id' => $prefix . 'equipment',
			'name' => __('Equipment', 'berlinmobil'),
			'type'  => 'text'
		));

        foreach(self::getEquipments() as $key => $equipment) {
            $cmb->add_field(array(
                'name' => $equipment,
                'id' => $prefix . $key,
                'type' => 'checkbox'
            ));
        }
	}

	public function show_trailer($field)
	{
		$trailers = get_posts(array(
			'post_type' => 'b-trailer',
			'posts_per_page' => -1,
			'lang' => pll_get_post_language($field->object_id)
		));

		$options = [];
		foreach ($trailers as $trailer) {
			$options[$trailer->ID] = $trailer->post_title;
		}

		return $options;

	}

	/**
	 * Registers admin columns to display. Hooked in via CPT_Core.
	 *
	 * @since  NEXT
	 * @param  array $columns Array of registered column names/labels.
	 * @return array          Modified array
	 */
	public function columns( $columns ) {
		$new_column = array(
			'seats' => __('Seating', 'berlinmobil'),
		);

		return array_merge($columns, $new_column);
	}

	/**
	 * Handles admin column display. Hooked in via CPT_Core.
	 *
	 * @since  NEXT
	 * @param array $column  Column currently being rendered.
	 * @param int   $post_id ID of post to display column for.
	 */
	public function columns_display( $column, $post_id ) {
		switch ( $column ) {
			case 'seats':
				echo get_post_meta($post_id, 'b_vehicle_seating', true);
				break;
		}
	}
}
