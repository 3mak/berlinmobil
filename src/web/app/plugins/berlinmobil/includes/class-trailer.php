<?php
/**
 * Berlinmobil Vehicle
 *
 * @version 0.0.1
 * @package Berlinmobil
 */

class B_Trailer extends CPT_Core {
	/**
	 * Parent plugin class
	 *
	 * @var class
	 * @since  NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 * Register Custom Post Types. See documentation in CPT_Core, and in wp-includes/post.php
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();

		// Register this cpt
		// First parameter should be an array with Singular, Plural, and Registered name.
		parent::__construct(
			array( __( 'Trailer', 'berlinmobil' ), __( 'Trailers', 'berlinmobil' ), 'b-trailer' ),
			array( 'supports' => array( 'title', 'thumbnail' ),  'public' => false)
		);
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
		add_action( 'cmb2_init', array( $this, 'fields' ) );
	}

	/**
	 * Add custom fields to the CPT
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function fields() {

		$prefix = 'b_trailer_';
		$cmb = new_cmb2_box(array(
			'id' => $prefix . 'metabox',
			'title' => __('Trailer Options', 'berlinmobil'),
			'object_types' => array('b-trailer')
		));

		$cmb->add_field(array(
			'id' => $prefix . 'number',
			'type' => 'text_small',
			'name' => __('Trailer Number', 'berlinmobil')
		));
	}

	/**
	 * Registers admin columns to display. Hooked in via CPT_Core.
	 *
	 * @since  NEXT
	 * @param  array $columns Array of registered column names/labels.
	 * @return array          Modified array
	 */
	public function columns( $columns ) {
		$new_column = array();
		return array_merge( $new_column, $columns );
	}

	/**
	 * Handles admin column display. Hooked in via CPT_Core.
	 *
	 * @since  NEXT
	 * @param array $column  Column currently being rendered.
	 * @param int   $post_id ID of post to display column for.
	 */
	public function columns_display( $column, $post_id ) {
		switch ( $column ) {
		}
	}
}
