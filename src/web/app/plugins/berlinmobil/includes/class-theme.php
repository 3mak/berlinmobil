<?php
/**
 * Berlinmobil Theme
 * @version 0.0.1
 * @package Berlinmobil
 */



class B_Theme {
	/**
	 * Parent plugin class
	 *
	 * @var    class
	 * @since  NEXT
	 */
	protected $plugin = null;

	/**
	 * Option key, and option page slug
	 *
	 * @var    string
	 * @since  NEXT
	 */
	protected $key = 'berlinmobil_theme';

	/**
	 * Options page metabox id
	 *
	 * @var    string
	 * @since  NEXT
	 */
	protected $metabox_id = 'berlinmobil_theme_metabox';

	/**
	 * Options Page title
	 *
	 * @var    string
	 * @since  NEXT
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Constructor
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();

		$this->title = __( 'Berlinmobil Theme', 'berlinmobil' );
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
	}

	/**
	 * Register our setting to WP
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function admin_init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page(
			$this->title,
			$this->title,
			'manage_options',
			$this->key,
			array( $this, 'admin_page_display' )
		);

		// Include CMB CSS in the head to avoid FOUC.
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2-options-page <?php echo esc_attr( $this->key ); ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}

	/**
	 * Add custom fields to the options page.
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function add_options_page_metabox() {

        $prefix = 'berlinmobil_';

		$cmb = new_cmb2_box( array(
			'id'         => $this->metabox_id,
			'hookup'     => false,
			'cmb_styles' => false,
			'show_on'    => array(
				// These are important, don't remove.
				'key'   => 'options-page',
				'value' => array( $this->key ),
			),
		) );


        $cmb->add_field( array(
            'name'    => __( 'Facebook URL', 'berlinmobil' ),
            'desc'    => __( 'field description (optional)', 'berlinmobil' ),
            'id'      => $prefix . 'facebook_url', // no prefix needed
            'type'    => 'text',
            'default' => __( 'http://facebook.com/', 'berlinmobil' ),
        ) );

        $cmb->add_field( array(
            'name'    => __( 'Instagram URL', 'berlinmobil' ),
            'desc'    => __( 'field description (optional)', 'berlinmobil' ),
            'id'      => $prefix . 'instagram_url', // no prefix needed
            'type'    => 'text',
            'default' => __( 'http://instagram.com/', 'berlinmobil' ),
        ) );

        $cmb->add_field( array(
            'name'    => __( 'Youtube URL', 'berlinmobil' ),
            'desc'    => __( 'field description (optional)', 'berlinmobil' ),
            'id'      => $prefix . 'youtube_url', // no prefix needed
            'type'    => 'text',
            'default' => __( 'http://youtube.com/', 'berlinmobil' ),
        ) );
	}
}
