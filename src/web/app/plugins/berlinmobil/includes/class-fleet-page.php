<?php
/**
 * Berlinmobil Fleet Page
 *
 * @since NEXT
 * @package Berlinmobil
 */

/**
 * Berlinmobil Fleet Page.
 *
 * @since NEXT
 */
class B_Fleet_Page {
	/**
	 * Parent plugin class
	 *
	 * @var   class
	 * @since NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
        add_action('cmb2_init', array($this, 'fields'));
        add_filter( 'cmb2_show_on', array($this, 'be_metabox_show_on_template'), 10, 2 );
	}


    function fields() {
        $prefix = 'b_fleet_site_';
        $cmb = new_cmb2_box(array(
            'id' => $prefix . 'meta_box',
            'title' => 'Fleet Options',
            'object_types'  => array( 'page' ),
            'show_on' => array(
                'key' => 'template',
                'value' => array('page-templates/fleet')
            )
        ));


        $cmb->add_field( array(
            'name'     => __('Show Fleet'),
            'desc'     => __('Show selected fleet'),
            'id'       => $prefix . 'show_fleet',
            'taxonomy' => 'b-fleet', //Enter Taxonomy Slug
            'type'     => 'taxonomy_select',
        ) );
    }

    /**
     * Metabox for Page Template
     * @author Kenneth White
     * @link https://github.com/WebDevStudios/CMB2/wiki/Adding-your-own-show_on-filters
     *
     * @param bool $display
     * @param array $meta_box
     * @return bool display metabox
     */
    public function be_metabox_show_on_template( $display, $meta_box ) {

        if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
            return $display;
        }

        if ( 'template' !== $meta_box['show_on']['key'] ) {
            return $display;
        }

        $post_id = 0;

        // If we're showing it based on ID, get the current ID
        if ( isset( $_GET['post'] ) ) {
            $post_id = $_GET['post'];
        } elseif ( isset( $_POST['post_ID'] ) ) {
            $post_id = $_POST['post_ID'];
        }

        if ( ! $post_id ) {
            return false;
        }

        $template_name = get_page_template_slug( $post_id );
        $template_name = ! empty( $template_name ) ? substr( $template_name, 0, -4 ) : '';

        // See if there's a match
        return in_array( $template_name, (array) $meta_box['show_on']['value'] );
    }



}
