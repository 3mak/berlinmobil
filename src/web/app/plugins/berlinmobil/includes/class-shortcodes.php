<?php
/**
 * Berlinmobil Shortcodes
 *
 * @since NEXT
 * @package Berlinmobil
 */

/**
 * Berlinmobil Shortcodes.
 *
 * @since NEXT
 */
class B_Shortcodes {
	/**
	 * Parent plugin class
	 *
	 * @var   class
	 * @since NEXT
	 */
	protected $plugin = null;

	/**
	 * Constructor
	 *
	 * @since  NEXT
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 *
	 * @since  NEXT
	 * @return void
	 */
	public function hooks() {
        add_shortcode('button', array($this, 'button_shortcode'));
        add_shortcode('icon', array($this, 'icon_shortcode'));
	}

    public function button_shortcode($atts, $content = null) {
        $atts = shortcode_atts(array(
            'url' => '#',
						'target' => ''
        ), $atts);

        return "<a href=\"{$atts['url']}\" target=\"{$atts['target']}\" class='round-button'>{$content}</a>";
    }

    public function icon_shortcode($atts, $content) {
        $atts = shortcode_atts(array(
            'icon' => 'default'
        ), $atts);

        return "<div class=\"icon-line\"><div class=\"sc-icon {$atts['icon']}\"></div>{$content}</div>";
    }
}
