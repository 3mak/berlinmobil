<?php
/**
 * Berlinmobil Fleet
 * @version 0.0.1
 * @package Berlinmobil
 */

class B_Fleet extends Taxonomy_Core
{
    /**
     * Parent plugin class
     *
     * @var   class
     * @since NEXT
     */
    protected $plugin = null;

    /**
     * Constructor
     *
     * @since  NEXT
     * @param  object $plugin Main plugin object.
     */
    public function __construct($plugin)
    {
        $this->plugin = $plugin;
        $this->hooks();

        parent::__construct(
            array(__('Fleet', 'berlinmobil'), __('Fleets', 'berlinmobil'), 'b-fleet'),
            array(
            ),
            array('b-vehicle')
        );
    }

    /**
     * Initiate our hooks
     *
     * @since  NEXT
     * @return void
     */
    public function hooks()
    {
        add_action('cmb2-taxonomy_meta_boxes', array($this, 'fields'));
	}

    public function fields() {

        $prefix = 'b_fleet_';

        $meta_boxes['fleet_metabox'] = array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Fleet Options', 'berlinmobil' ),
            'object_types'  => array( 'b-fleet' ),
            'fields' => array(
                array(
                    'id' => $prefix . 'form_shortcode',
                    'name' => __('Request Form Shortcode', 'berlinmobil'),
                    'type' => 'text'
                )
            ));

        return $meta_boxes;
    }
}
