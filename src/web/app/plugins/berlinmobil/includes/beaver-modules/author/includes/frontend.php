<?php
/**
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 */
?>

<div class="author-module">
    <div class="author-header">
        <div class="author-image">
            <?php echo wp_get_attachment_image($settings->photo, 'author-thumbnail'); ?>
        </div>
        <div class="author-text">
            <div class="author-first-row">
                <h6 class="author-name"><?php echo $settings->name; ?></h6>
                <div class="lang-icons">
                    <?php foreach (explode(',', $settings->lang) as $lang): ?>
                        <img src="<?php echo $module->url . 'assets/' . $lang . '.png' ?>" alt="">
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="author-position"><?php echo $settings->position; ?></div>
        </div>
    </div>
    <div class="author-biography">
        <?php echo nl2br($settings->bio); ?>
        <div class="row">
            <div class="large-7 columns author-csv">
                <div class="a-icon"><i class="fa fa-phone"></i><?php echo $settings->tel; ?></div>
                <div class="a-icon"><i class="fa fa-fax"></i><?php echo $settings->fax; ?></div>
                <div class="a-icon"><i class="fa fa-paper-plane"></i><a href="mailto:<?php echo $settings->email; ?>"><?php echo $settings->email; ?></a></div>
            </div>
            <div class="large-5 columns">
                <div class="qr-wrapper">
                    <div class="qr-image">
                        <a target="_blank" href="<?php echo wp_get_attachment_url($settings->qr)?>"><?php echo wp_get_attachment_image($settings->qr, 'author-thumbnail'); ?></a>
                    </div>
                    <div class="qr-text">
                        <?php _e('Scan QR<br> Code', 'berlinmobil')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
