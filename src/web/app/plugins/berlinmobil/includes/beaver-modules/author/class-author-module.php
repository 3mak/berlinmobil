<?php
/**
 * berlinmobil Author Module
 * @version 0.0.0
 * @package berlinmobil
 */

class B_Author_Module extends FLBuilderModule {

    /**
     * Constructor
     *
     * @since  NEXT
     */
	public function __construct() {

        parent::__construct(array(
            'name' => __('Team Member', 'berlinmobil'),
            'description' => __('Team Member', 'berlinmobil'),
            'category' => __('Berlinmobil Modules', 'berlinmobil'),
            'dir' => BERLINMOBIL_MODULES_DIR . 'author/',
            'url' => BERLINMOBIL_MODULES_URL . 'author/'
        ));
        
	}
}


FLBuilder::register_module('B_Author_Module', array(
    'general' => array(
        'title' => __('General', 'berlinmobil'),
        'sections' => array(
            'author' => array(
                'title' => __('Author', 'berlinmobil'),
                'fields' => array(
                    'name' => array(
                        'type' => 'text',
                        'label' => 'Name',
                    ),
                    'position' => array(
                        'type' => 'text',
                        'label' => 'Position',
                    ),
                    'email' => array(
                        'type' => 'text',
                        'label' => 'Email',
                    ),
                    'tel' => array(
                        'type' => 'text',
                        'label' => 'Tel',
                    ),
                    'fax' => array(
                        'type' => 'text',
                        'label' => 'Fax',
                    ),
                    'photo' => array(
                        'type' => 'photo',
                        'label' => 'Photo',
                    ),
                    'bio' => array(
                        'type' => 'editor',
                        'label' => 'Biography',
                    ),
                    'lang' => array(
                        'type' => 'text',
                        'label' => 'Languages',
                        'help' => 'separate with comma: de,en'
                    ),
                    'csv' => array(
                        'type' => 'link',
                        'label' => 'Link to CSV'
                    ),
                    'qr' => array(
                        'type' => 'photo',
                        'label' => 'QR Code'
                    )
                )
            )
        )
    )
));
