<?php

/**
 * @class FL_Heading
 */

class B_Heading_Beaver_Module extends FLBuilderModule {

    public $plugin;
	/**
	 * @method __construct
	 */
	public function __construct() {

		parent::__construct(array(
			'name'          => __( 'Heading 2', 'berlinmobil' ),
			'description'   => __( 'Display a styled heading.', 'berlinmobil' ),
			'category'		=> __( 'Berlinmobil Modules', 'berlinmobil' ),
			'dir'           => BERLINMOBIL_MODULES_DIR . '/header',
			'url'           => BERLINMOBIL_MODULES_URL . '/header',
		));

	}

}


/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('B_Heading_Beaver_Module', array(
	'general'       => array(
		'title'         => __( 'General', 'berlinmobil' ),
		'sections'      => array(
			'general'       => array(
				'title'         => __( 'Styling', 'berlinmobil' ),
				'fields'        => array(
					'heading_tag'   => array(
						'type'      => 'select',
						'label'     => __( 'Heading Tag', 'berlinmobil' ),
						'options'   => array(
							'h1'        => __( 'H1', 'berlinmobil' ),
							'h2'		=> __( 'H2', 'berlinmobil' ),
							'h3'		=> __( 'H3', 'berlinmobil' ),
							'h4'		=> __( 'H4', 'berlinmobil' ),
						)
					),
					'heading_align'  => array(
						'type'      => 'select',
						'label'     => __( 'Heading Alignment', 'berlinmobil' ),
						'options'   => array(
							''              => __( 'None', 'berlinmobil' ),
							'alignleft'     => __( 'Left', 'berlinmobil' ),
							'aligncenter'   => __( 'Center', 'berlinmobil' ),
							'alignright'    => __( 'Right', 'berlinmobil' ),
						)
					),
					'has_ruler' => array(
						'type'      => 'select',
						'label'     => __( 'Insert Ruler?', 'berlinmobil' ),
						'options'   => array(
							'no'   => __( 'No', 'berlinmobil' ),
							'yes'    => __( 'Yes', 'berlinmobil' ),
						),
					),
                    'color' => array(
                        'type' => 'select',
                        'label' => __('Font Color', 'berlinmobil'),
                        'options' => array(
                            'primary' => 'primary',
                            'secondary' => 'secondary',
                            'black' => 'black'
                        )
                    )
				)
			),
			'content'	=> array(
				'title'		=> __( 'Title', 'berlinmobil' ),
				'fields'	=> array(
					'title_text'   => array(
						'type'      => 'text',
						'label'     => __( 'Title Text', 'berlinmobil' ),
					),
					'has_subtitle' => array(
						'type'      => 'select',
						'label'     => __( 'Insert Subtitle?', 'berlinmobil' ),
						'options'   => array(
							'no'   => __( 'No', 'berlinmobil' ),
							'yes'    => __( 'Yes', 'berlinmobil' ),
						),
                        'toggle'     => array(
                            'no'     => array(
                            	'fields'	=> array()
                            ),
                            'yes'    => array(
                                'fields'	=> array( 'subtitle_text')
                            )
                        )
					),
					'subtitle_text'   => array(
						'type'      => 'text',
						'label'     => __( 'Sutitle Text', 'berlinmobil' ),
					),
				)
			),
		)
	)
));