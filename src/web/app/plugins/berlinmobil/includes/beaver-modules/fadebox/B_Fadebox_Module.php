<?php

/**
 * Created by PhpStorm.
 * User: emak
 * Date: 19.05.16
 * Time: 16:00
 */
class B_Fadebox_Module extends FLBuilderModule
{

    public function __construct()
    {
        parent::__construct(array(
            'name' => __('Fadebox', 'berlinmobil'),
            'description' => __('Fadebox', 'berlinmobil'),
            'category' => __('Berlinmobil Modules', 'berlinmobil'),
            'dir' => BERLINMOBIL_MODULES_DIR . 'fadebox/',
            'url' => BERLINMOBIL_MODULES_URL . 'fadebox/'
        ));
    }
}

FLBuilder::register_module(B_Fadebox_Module::class, array(
    'general' => array(
        'title' => 'General',
        'sections' => array(
            'general' => array(
                'title' => 'General',
                'fields' => array(
                    'title' => array(
                        'type' => 'text',
                        'label' => 'Title'
                    ),
                    'content' => array(
                        'type' => 'editor',
                        'label' =>  'Content'
                    )
                )
            )
        )
    )
));