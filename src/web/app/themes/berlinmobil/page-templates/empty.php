<?php
/*
Template Name: Empty
*/
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/favicon.ico"
          type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144"
          href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon"
          href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon.png">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php while (have_posts()) : the_post(); ?><?php the_content(); ?><?php endwhile; ?>
<?php wp_footer(); ?>
<script type="text/javascript">
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-81229028-1', 'auto');
	ga('send', 'pageview');
</script>
<?php do_action('foundationpress_before_closing_body'); ?>
</body>
</html>
