<?php

function foundationpress_theme_customizer($wp_customize)
{
	$wp_customize->add_setting('foundationpress_parallax_1'); // Add setting for logo uploader
	$wp_customize->add_setting('foundationpress_parallax_2'); // Add setting for logo uploader

	// Add control for logo uploader (actual uploader)
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'foundationpress_parallax_1', array(
		'label' => __('Parallax 1', 'foundationpress'),
		'section' => 'background_image',
		'settings' => 'foundationpress_parallax_1',
	)));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'foundationpress_parallax_2', array(
		'label' => __('Parallax 2', 'foundationpress'),
		'section' => 'background_image',
		'settings' => 'foundationpress_parallax_2',
	)));
}

add_action('customize_register', 'foundationpress_theme_customizer');
