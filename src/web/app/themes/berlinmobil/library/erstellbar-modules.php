<?php
/**
 * Erstellbar Beaver Module Ext.
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

if (!function_exists('berlinmobile_display_vehicle_info')) {
	function berlinmobile_display_vehicle_info()
	{
		echo '<div class="vehicle-seating">' . get_post_meta(get_the_ID(), 'b_vehicle_seating', true) . '</div>';
	}

	add_action('erstellbar_after_slick_slider_content', 'berlinmobile_display_vehicle_info');
}

