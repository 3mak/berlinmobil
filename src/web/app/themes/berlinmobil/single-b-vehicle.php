<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
    <div id="single-vehicle" role="main">
		<?php do_action('foundationpress_before_content'); ?>
		<?php while (have_posts()) : the_post(); ?>
            <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                <header>
                    <h1 class="entry-title"><?php _e('Anfrage', 'foundationpress') ?>: <?php the_title(); ?></h1>
                </header>
                <div class="row">
                    <div class="medium-4 columns">
						<?php do_action('foundationpress_post_before_entry_content'); ?>
                        <div class="entry-content">
							<?php
							if (has_post_thumbnail()) :
								the_post_thumbnail();
							endif;
							?>
                            <div class="seatings speech-box">
                                <div class="speech-box-header">
									<?php _e('Seatings', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_seating', true); ?>
                                </div>
                            </div>
                            <div class="equipment speech-box">
                                <div class="speech-box-header">
									<?php _e('Equipment', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_equipment', true); ?>
                                    <div class="equipment-icons">
										<?php $eq_values = get_equipments(get_the_ID()); ?>
										<?php foreach ($eq_values as $equipment => $equipment_available) : ?>
                                            <div class="icon <?php echo ($equipment_available) ? $equipment : 'invisible' ?>"></div>
										<?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="suitability speech-box">
                                <div class="speech-box-header">
									<?php _e('Suitable for:', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_suitability', true); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="medium-8 columns">
						<?php $fleet = wp_get_post_terms(get_the_ID(), array('b-fleet')) ?><?php $form = get_term_meta($fleet[0]->term_id, 'b_fleet_form_shortcode', true); ?><?php echo do_shortcode($form); ?>
                    </div>
                </div>
            </article>
		<?php endwhile; ?>

		<?php do_action('foundationpress_after_content'); ?>
    </div>
<?php get_footer();
