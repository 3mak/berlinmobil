'use strict';

(function () {
	var $ = jQuery;
	var offset = $('nav#site-navigation').offset().top;
	var scrolled = function () {
		var scrollTop = $(window).scrollTop();
		if (scrollTop >= offset) {
			$('body').addClass('scrolled');
		} else {
			$('body').removeClass('scrolled');
		}
	};


	scrolled();

	$(window).scroll(function () {
		scrolled();
	});
})();