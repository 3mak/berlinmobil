var $ = jQuery;
$(document).ready(function () {
	$('.parallax').each(function () {
		var $bgobj = $(this); // assigning the object
		var topPos = $bgobj.css('background-position').split(' ');
		var yPosCss = topPos[1].slice(0, -2);

		function parallax() {
			var yPos = -($(window).scrollTop() / 4);
			yPos = parseInt(yPosCss) + yPos;

			// Put together our final background position
			var coords = '50% ' + yPos + 'px';

			// Move the background
			$bgobj.css({backgroundPosition: coords});
		}

		$(window).scroll(function () {
			requestAnimationFrame(parallax);
		});
	});
});
