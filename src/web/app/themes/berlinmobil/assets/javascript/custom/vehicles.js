/**
 * Created by emak on 04.05.16.
 */
(function () {
    var $ = jQuery;
	var vehicle_cont = $('ul.vehicles');
	setSubmenuWidth();

	$(window).resize(function () {
		requestAnimationFrame(setSubmenuWidth);
	});

	function setSubmenuWidth() {
		var ex_item = $(vehicle_cont).find('.is-expanded');

		if (ex_item.length !== 0) {
			var current_sub_menu = $(ex_item).find('.submenu');
			$(current_sub_menu).css('margin-left', 0);
			var delta = $(vehicle_cont).offset().left - $(current_sub_menu).offset().left;
			var width = $(vehicle_cont).width();

			$(current_sub_menu).width(width).css('margin-left', delta);
		}

	}

	$('ul.vehicles .vehicle>.vehicle-content').click(function () {
		var $expandableItem = $(this).parent();
		$expandableItem.toggleClass('is-expanded');
		$expandableItem.siblings().removeClass('is-expanded');
		//$(this).find('.submenu').slideDown('slow');

		if ($('.vehicle-slider', $expandableItem).length !== 0) {
			$('.vehicle-slider', $expandableItem).get(0).slick.setPosition();
		}
		setSubmenuWidth();
	});

	$('.vehicle-slider').slick({
		dots: true
	});
})();
