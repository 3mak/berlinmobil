<?php
/**
 * The template for displaying the header.
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @since FoundationPress 1.0.0
 */
?>
    <!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/favicon.ico"
              type="image/x-icon">
        <link rel="apple-touch-icon" sizes="144x144"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="114x114"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon"
              href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/apple-touch-icon.png">
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
<?php do_action('foundationpress_after_body'); ?><?php if (get_theme_mod('wpt_mobile_menu_layout') == 'offcanvas') : ?>
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <?php get_template_part('template-parts/mobile-off-canvas'); ?>
<?php endif; ?><?php do_action('foundationpress_layout_start'); ?>
    <header class="b-header">
        <nav class="b-header__navigation">
            <div class="b-header__navigation__wrapper" data-js-atom="navigation">
                <div class="b-header__hamburger" data-toggle="mobile-menu"></div>
                <div class="b-header__logo"><a href="<?= home_url() ?>"></a></div>
                <?php foundationpress_top_bar_r(); ?>

                <?php if (!get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') == 'topbar') : ?><?php get_template_part('template-parts/mobile-top-bar'); ?><?php endif; ?>
                	<?php if(function_exists('pll_the_languages')): ?>
					<?php $translations = pll_the_languages(array('raw' => 1)); ?>
                	<?php $is_default = pll_default_language() == pll_current_language(); ?>
				<?php endif; ?>
            </div>
        </nav>
    </header>
<?php if(function_exists('pll_the_languages')): ?>
    <section class="c-section--top row">
        <div class="small-12 columns">
            <div class="b-switch <?php echo ($is_default) ? 'no-active' : 'is-active' ?>">
                <div class="pill"></div>
                <div class="text">
                    <?php foreach ($translations as $translation): ?><?php //var_dump($translation); ?>
                        <a href="<?php echo $translation['url']; ?>"
                           class="<?php echo $translation['current_lang'] ? 'is-active' : 'not-active' ?>"><?php echo $translation['slug']; ?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
	<?php endif; ?>
    <section class="container">
<?php do_action('foundationpress_after_header');
