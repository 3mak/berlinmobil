<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
</section>
<div id="footer-container">
    <section id="before-footer">
		<?php BB_Header_Footer::get_footer_content(); ?>
    </section>
</div>
<?php do_action('foundationpress_layout_end'); ?>

<?php if (get_theme_mod('wpt_mobile_menu_layout') == 'offcanvas') : ?>
    </div><!-- Close off-canvas wrapper inner -->    </div><!-- Close off-canvas wrapper --></div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<script type="text/javascript">
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-81229028-1', 'auto');
	ga('send', 'pageview');
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8sK8Zb9haaMSroz2vwRHfUrdhS0gfRSY&libraries=places"
        type="text/javascript"></script>
<script type="text/javascript">
	function initialize() {
		var input = document.querySelectorAll('.is-google');
		for (var i = 0; i < input.length; i++) {
			new google.maps.places.Autocomplete(input[i]);
		}
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php do_action('foundationpress_before_closing_body'); ?>
<div class="b-floating-button"><a class="floating-button__anchor" href="<?php bloginfo('url') ?>">Jetzt Buchen</a>
</div></body></html>
