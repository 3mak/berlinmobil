<?php
/*
Template Name: Fleet
*/
get_header(); ?><?php get_template_part('template-parts/featured-image'); ?>

    <div id="page-fleet" role="main">

<?php do_action('foundationpress_before_content'); ?><?php while (have_posts()) : the_post(); ?><?php $fleet = wp_get_object_terms(get_the_ID(), 'b-fleet'); ?>

<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
	<?php do_action('foundationpress_page_before_entry_content'); ?>
    <div class="entry-content">
		<?php the_content(); ?>
    </div>
    <section class="fleet">
    <ul class="vehicles">
	<?php foreach (get_vehicles($fleet) as $vehicle) : ?>
    <li class="vehicle <?php echo $vehicle->post_name ?>">
        <div class="vehicle-content">
            <div class="vehicle-image">
				<?php echo get_the_post_thumbnail($vehicle->ID) ?>
            </div>
            <h3 class="vehicle-title">
				<?php echo $vehicle->post_title; ?>
            </h3>
            <div class="vehicle-seating">
				<?php echo get_post_meta($vehicle->ID, 'b_vehicle_seating', true); ?>
				<?php foreach (get_trailers_by_ids(get_post_meta($vehicle->ID, 'b_vehicle_trailer_ids', true)) as $trailer): ?>
                    <span class="badge"><?php echo get_post_meta($trailer->ID, 'b_trailer_number', true) ?></span>
				<?php endforeach; ?>
            </div>
        </div>
        <ul class="submenu">
        <li class="animation-frame"><h3 class="vehicle-title"><?php echo $vehicle->post_title; ?></h3>
        <div class="row">
        <div class="medium-5 columns">
            <div class="seatings speech-box">
                <div class="speech-box-header">
					<?php _e('Seatings', 'foundationpress'); ?>
                </div>
                <div class="speech-box-content">
					<?php echo get_post_meta($vehicle->ID, 'b_vehicle_seating', true); ?>
                </div>
            </div>
            <div class="equipment speech-box">
                <div class="speech-box-header">
					<?php _e('Equipment', 'foundationpress'); ?>
                </div>
                <div class="speech-box-content">
					<?php echo get_post_meta($vehicle->ID, 'b_vehicle_equipment', true); ?>
                    <div class="equipment-icons">
						<?php $eq_values = get_equipments($vehicle->ID); ?>
						<?php foreach ($eq_values as $equipment => $equipment_available) : ?>
                            <div class="icon <?php echo ($equipment_available) ? $equipment : 'invisible' ?>"></div>
						<?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="suitability speech-box">
                <div class="speech-box-header">
					<?php _e('Suitable for:', 'foundationpress'); ?>
                </div>
                <div class="speech-box-content">
					<?php echo get_post_meta($vehicle->ID, 'b_vehicle_suitability', true); ?>
                </div>
            </div>
        </div>
        <div class="medium-7 columns">


		<?php $live_photo_slider = get_post_meta($vehicle->ID, 'live_photo_slider', true); ?><?php if (!empty($live_photo_slider)): ?>
            <div class="vehicle-slider erstellbar-slider">
				<?php foreach ($live_photo_slider as $slide): ?>
                    <div class="vehicle-slide">
                        <img src="<?php echo $slide['b_vehicle_live_photo_slide'] ?>">
                    </div>
				<?php endforeach; ?>
            </div>
		<?php else: ?><?php $live_photo = get_post_meta($vehicle->ID, 'b_vehicle_live_photo', true); ?><?php if (!empty($live_photo)): ?>
            <img class="live-photo" src="<?php echo $live_photo; ?>" alt=""/>
		<?php endif; ?><?php endif; ?><?php echo get_the_post_thumbnail($vehicle->ID) ?><?php $set_view_image = get_post_meta($vehicle->ID, 'b_vehicle_seat_view', true); ?>
        <<<<<<< HEAD:src/web/app/themes/berlinmobil/page-templates/fleet.php
		<?php if (!empty($set_view_image)): ?>
            <img src="<?php echo $set_view_image ?>" alt="Seat View">=======
			<?php if (!empty($set_view_image)) : ?>
                <img src="<?php echo $set_view_image ?>" alt="Seat View">
			<?php endif; ?><?php $panorama_link = get_post_meta($vehicle->ID, 'b_vehicle_panolink', true); ?><?php if (!empty($panorama_link)): ?>
                <div class="speech-box is-panoview">
                    <div class="speech-box-content">
                        <div class="speech-box-icon is-360"></div>
                        <a class="button isnt-margin" target="_blank"
                           href="<?php echo $panorama_link; ?>"><?php _e('Inside view ›', 'foundationpress'); ?></a>
                    </div>
                </div>>>>>>>> bc9566cd020ca8ba20e50bf14c751341e377a288:web/app/themes/berlinmobil/page-templates/fleet.php
			<?php endif; ?>
            <a href="<?php echo sprintf('/b-vehicle/%s/%s', $fleet[0]->slug, $vehicle->post_name); ?>"
               class="round-button large">
				<?php _e('Request for bus', 'foundationpress') ?> › </a></div></div></li></ul></li>
			<?php endforeach; ?>
            </ul></section>
            <section id="trailer">
                <div class="small-12 columns">
                    <h2 class="trailer-main-title">
						<?php _e('To hang - Our trailer', 'foundationpress'); ?>
                    </h2>
                    <div class="trailer-container">
						<?php foreach (get_posts(array('post_type' => 'b-trailer', 'lang' => pll_current_language())) as $trailer) : ?>
                            <div class="trailer">
								<?php echo get_the_post_thumbnail($trailer->ID); ?>
                                <h3 class="trailer-title"><?php echo $trailer->post_title; ?> <span
                                            class="badge"><?php echo get_post_meta($trailer->ID, 'b_trailer_number', true) ?></span>
                                </h3>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </section></article>
			<?php endwhile; ?><?php do_action('foundationpress_after_content'); ?>
            </div>
			<?php get_footer();
