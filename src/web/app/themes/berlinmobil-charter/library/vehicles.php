<?php
/**
 * Vehicle Wp Queries
 *
 * @author   Sven Friedemann <sven@ertellbar.de>
 * @licence  MIT
 * @package  Foundationpress
 */

/**
 * Get Vehicles
 *
 * @return array
 */
function get_vehicles()
{

	$query = new WP_Query(array(
		'post_type' => 'b-vehicle',
		'posts_per_page' => -1,
	));

	return $query;
}


/**
 * Get Equipment be vehicle
 *
 * @param $vehicle_id
 * @return array
 */
function get_equipments($vehicle_id)
{

	$equipments = B_Vehicle::getEquipments();
	$equipment_values = [];
	foreach ($equipments as $key => $equipment) {
		$equipment_values[$key] = !empty(get_post_meta($vehicle_id, 'b_vehicle_' . $key, true));
	}

	return $equipment_values;
}

/**
 * Get Trailer by ID
 */
function get_trailers_by_ids($trailerIds)
{

	if (empty($trailerIds)) {
		return array();
	}

	$wp_query = new WP_Query(array(
		'post__in' => array_values($trailerIds),
		'post_type' => 'b-trailer'
	));

	return $wp_query->get_posts();

}

/**
 * View Filters in Form if available
 */
function cf7_dynamic_select_do_trailer($choices, $args = array())
{
	global $post;
	$choices = [];
	$trailerIds = get_post_meta($post->ID, 'b_vehicle_trailer_ids', true);

	if (!empty($trailerIds)) {
		$trailers = get_trailers_by_ids($trailerIds);

		if (!empty($trailers)) {
			$choices['---'] = '---';
		}

		foreach ($trailers as $trailer) {
			$choices[$trailer->post_title] = $trailer->post_title;
		}
	}

	return $choices;
}

add_filter('trailer', 'cf7_dynamic_select_do_trailer', 10, 2);


/**
 *  Rewrite Permalinks for Slider
 */
function rewrite_permalink($value, $post, $settings)
{
	$fleet = get_term($settings->{'tax_b-vehicle_b-fleet'});

	return sprintf('/b-vehicle/%s', $post->post_name);
}

// add_filter('erstellbar_slider_item_permalink', 'rewrite_permalink', 10, 3);


/**
 * Rewrite permalinks for Language Switcher
 */
function language_link_query_args($url, $slug)
{
	$fleet_slug = get_query_var('b-fleet');
	if (!empty($fleet_slug)) {
		$segments = explode('/', $url);
		$fleet_pos = count($segments) - 3;

		$current_fleet = get_term_by('slug', $fleet_slug, 'b-fleet');
		$fleet_translation = get_term(pll_get_term($current_fleet->term_id, $slug));
		$segments[$fleet_pos] = $fleet_translation->slug;

		return implode('/', $segments);
	}

	return $url;
}

add_filter('pll_the_language_link', 'language_link_query_args', 10, 2);
