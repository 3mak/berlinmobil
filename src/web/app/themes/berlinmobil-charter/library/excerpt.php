<?php
/**
 * Excerpt Options
 *
 * @licence MIT
 * @author Sven Friedemann
 * @package Foundationpress
 */

/**
 * Set New Excerpt Length
 *
 * @param $length
 * @return int
 */
function new_excerpt_length($length)
{
	return 20;
}

add_filter('excerpt_length', 'new_excerpt_length');