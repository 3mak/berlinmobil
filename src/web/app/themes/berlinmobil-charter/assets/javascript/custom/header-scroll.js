'use strict';

(function () {
	var $ = jQuery;
	var offset = $('[data-js-atom=navigation]').offset().top;
	var scrolled = function () {
		var scrollTop = $(window).scrollTop();
		if (scrollTop > offset) {
			$('body').addClass('is-scrolled');
		} else {
			$('body').removeClass('is-scrolled');
		}
	};


	scrolled();

	$(window).scroll(function () {
		scrolled();
	});
})();