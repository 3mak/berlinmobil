/**
 * Created by emak on 04.05.16.
 */
(function () {
	$ = jQuery;
	$('.vehicle-slider').slick({
		dots: true,
		autoplay: true
	});

	$('#single-vehicle-navigation').slick({
		dots: true,
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});
})();
