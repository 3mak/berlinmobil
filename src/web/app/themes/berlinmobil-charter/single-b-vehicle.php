<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
    <div id="single-vehicle" role="main">
		<?php do_action('foundationpress_before_content'); ?>
		<?php while (have_posts()) : the_post(); ?>
            <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div class="row">
                    <div class="medium-5 columns">
						<?php do_action('foundationpress_post_before_entry_content'); ?>
                        <div class="entry-content">
                            <div class="seatings speech-box">
                                <div class="speech-box-header">
									<?php _e('Seatings', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_seating', true); ?>
                                </div>
                            </div>
                            <div class="equipment speech-box">
                                <div class="speech-box-header">
									<?php _e('Equipment', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_equipment', true); ?>
                                    <div class="equipment-icons">
										<?php $eq_values = get_equipments(get_the_ID()); ?>
										<?php foreach ($eq_values as $equipment => $equipment_available) : ?>
                                            <div class="icon <?php echo ($equipment_available) ? $equipment : 'invisible' ?>"></div>
										<?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="suitability speech-box">
                                <div class="speech-box-header">
									<?php _e('Suitable for:', 'foundationpress'); ?>
                                </div>
                                <div class="speech-box-content">
									<?php echo get_post_meta(get_the_ID(), 'b_vehicle_suitability', true); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="medium-7 columns">
						<?php $live_photo_slider = get_post_meta(get_the_ID(), 'live_photo_slider', true); ?>
						<?php if (!empty($live_photo_slider)): ?>
                            <div class="vehicle-slider erstellbar-slider c-slider--vehicle">
								<?php foreach ($live_photo_slider as $slide): ?>
                                    <div class="vehicle-slide">
                                        <img src="<?php echo $slide['b_vehicle_live_photo_slide'] ?>">
                                    </div>
								<?php endforeach; ?>
                            </div>
						<?php else: ?><?php $live_photo = get_post_meta(get_the_ID(), 'b_vehicle_live_photo', true); ?><?php if (!empty($live_photo)): ?>
                            <img class="live-photo" src="<?php echo $live_photo; ?>" alt=""/>
						<?php endif; ?><?php endif; ?>


						<?php echo get_the_post_thumbnail(get_the_ID()) ?>
						<?php $set_view_image = get_post_meta(get_the_ID(), 'b_vehicle_seat_view', true); ?>
						<?php if (!empty($set_view_image)) : ?>
                            <img src="<?php echo $set_view_image ?>" alt="Seat View">
						<?php endif; ?>

						<?php $panorama_link = get_post_meta(get_the_ID(), 'b_vehicle_panolink', true); ?>
						<?php if (!empty($panorama_link)): ?>
                            <div class="speech-box is-panoview">
                                <div class="speech-box-content">
                                    <div class="speech-box-icon is-360"></div>
                                    <a class="button isnt-margin" target="_blank"
                                       href="<?php echo $panorama_link; ?>"><?php _e('Inside view ›', 'foundationpress'); ?></a>
                                </div>
                            </div>
						<?php endif; ?>
                    </div>
                </div>
            </article>
		<?php endwhile; ?>

		<?php do_action('foundationpress_after_content'); ?>
    </div>
    <section class="single-vehicle-navigation">
        <div class="row">
            <div class="medium-12 columns single-vehicle-navigation">
                <h1 class="heading aligncenter  black"><? _e('Welcher Bus darf Sie ans Ziel bringen?') ?></h1>
                <p style="text-align: center;"><?php _e('Über 50 moderne Busse der Premium-Marken Mercedes und Setra stehen für Sie bereit. In jeder Größe, von 8-69 Sitzplätzen.') ?></p>
                <div class="c-slider c-slider--bus" id="single-vehicle-navigation">
					<?php $query = get_vehicles() ?>
					<?php if ($query->have_posts()): ?><?php while ($query->have_posts()): $query->the_post(); ?>
                        <div class="vehicle-slide">
                            <a href="<?php the_permalink() ?>">
								<?php the_post_thumbnail(); ?>
                            </a> <a href="<?php the_permalink() ?>">
                                <h5 class="erstellbar-slides-title"><?php the_title() ?></h5>
                            </a> <a href="<?php the_permalink() ?>">
                                <div class="slider__vehicle-seating"><?php echo get_post_meta(get_the_ID(), 'b_vehicle_seating', true) ?><?php _e('Personen'); ?>
                                </div>
								<?php if (get_post_meta(get_the_ID(), 'b_vehicle_toilett', true)): ?>
                                    <div class="c-badge">WC</div>
								<?php endif; ?>
                            </a>
                        </div>
					<?php endwhile; ?><?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();
